from random import randint
name = input("Hi! What is your name?\n")

month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

attempt = 5

for num in range(attempt):
    print("Guess ", num+1, ":", name, " were you born in ", month[randint(0,11)], "/", randint(1924,2004))
    guess = input("yes or no?\n")
    if (guess == "yes"):
        print("\nI knew it!")
        exit()
    elif (guess == "no" and num == 4):
        print("\nI have other things to do. Good bye.")
        exit()
    elif (guess == "no"):
        print("\nDrat Lemme try again!\n")
    else:
        print("\nPlease type \"yes\" or \"no\"\n")